import { requestLogin } from "./request";

export function loginRes(account, password, phone) {
  phone = phone || 0
  let config = {
    method: 'post',
    url:  '/api/login',
    params: {
      account,
      password,
      phone
    }
  }
  if (phone) config.params.phone = phone
  
  return  requestLogin(config)
}

export function checkUser (userName, password) {
      return  requestLogin({
      url: '/api/check',
      method: 'get',
      params: {
        userName,
        password
      }
    })
}

export function storeInfo(info) {
  if(info.gender == '男') info.gender = 1
  if(info.gender == '女') info.gender = 0
  return  requestLogin({
    url: '/store',
    method: 'get',
    params: {
      userAccount:window.localStorage.userName,
      username:info.name,
      gender:info.gender,
      attach:info.attach,
      desc:info.desc,
      key:window.localStorage.paykey
    }
  })
}
export function pay(order,gender,type) {
  return  requestLogin({
    url: '/pay',
    method: 'get',
    params: {
       order:order,
       gender:gender,
       type:type
    }
  })
}

export function md5(sign) {
  return  requestLogin({
    url: '/api/md5',
    method: 'get',
    params: {
       sign:sign
    }
  })

}

export function getorder(order) {
  return  requestLogin({
    url: '/api/getorder',
    method: 'get',
    params: {
       order
    }
  })

}

export function getmyorder(order) {
  return  requestLogin({
    url: '/api/getmyorder',
    method: 'get',
    params: {
       order
    }
  })

}