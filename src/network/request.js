import axios from "axios"

export function request(config) {
  // 拦截器
 
}

export function requestLogin (config) {

  const instance = axios.create({
    baseURL: "https://yzx020530.xyz/apis",
    timeout: 10000,
  })
  instance.interceptors.request.use(config => {
    let token = window.localStorage.token
    if (token) {  // 判断是否存在token，如果存在的话，则每个http header都加上token
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config
  }) 
  return instance(config)
}